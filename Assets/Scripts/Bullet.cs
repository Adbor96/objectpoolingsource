﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour, IPooledObjects
{
    public float upforce = 1;
    public float sideforce = .1f;

    public event EventHandler OnPlayerImpact;
    // Start is called before the first frame update
    public void OnObjectSpawn()
    {
        float xForce = UnityEngine.Random.Range(-sideforce, sideforce);
        float yForce = UnityEngine.Random.Range(upforce / 2, upforce);
        float zForce = UnityEngine.Random.Range(-sideforce, sideforce);

        Vector3 force = new Vector3(xForce, -yForce, zForce);

        GetComponent<Rigidbody>().velocity = force;

    }

    // Update is called once per frame
    void Update()
    {

    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
                 //OnPlayerImpact?.Invoke(this, EventArgs.Empty);
            PlayerMovement player = FindObjectOfType<PlayerMovement>();
            if (!player.isInvincible)
            {
                player.LoseHealth(1);
                gameObject.SetActive(false);
            }
            else
            {
                return;
            }
            //ATT GÖRA
            //varför triggas eventet flera gånger?
        }
    }
}
