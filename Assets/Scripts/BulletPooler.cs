﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletPooler : MonoBehaviour
{

    [System.Serializable]
    public class Pool
    {
        public string tag; //poolens tag
        public GameObject prefab; //prefaben av det som ska poolas
        public int size; //storleken av poolen aka hur många objekt det ska finnas i poolen
    }

    public static BulletPooler Instance;

    #region Singleton
    private void Awake()
    {
        Instance = this;
    }
    #endregion
    public List<Pool> poolsList; //lista över pools
    public Dictionary<string, Queue<GameObject>> poolDictionary; //håller koll på taggen av det som ska poolas samt vilket gameobject

    // Start is called before the first frame update
    void Start()
    {
        poolDictionary = new Dictionary<string, Queue<GameObject>>();

        foreach (Pool pool in poolsList)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>(); //kö av kulans gameobject

            for (int i = 0; i < pool.size; i++) //fyll ut poolen genom att instantiate så många objekt som specifierat i "size"
            {
               GameObject obj = Instantiate(pool.prefab); //instantiate:a kulan
                obj.SetActive(false); //set kulan som inaktiv
                objectPool.Enqueue(obj); //sätt kulan i kön
            }
            poolDictionary.Add(pool.tag, objectPool);
        }
    }
    public GameObject SpawnFromPoll(string tag, Vector3 position, Quaternion rotation)
    {
        if (!poolDictionary.ContainsKey(tag)) //om dictionary får en tag som den inte har en pool för
        {
            Debug.LogWarning("Pool med tag " + tag + " finns inte.");
            return null;
        }

        GameObject objectToSpawn = poolDictionary[tag].Dequeue();

        objectToSpawn.SetActive(true); //sätt kulan som aktiv
        objectToSpawn.transform.position = position;
        objectToSpawn.transform.rotation = rotation;

        IPooledObjects pooledObjects = objectToSpawn.GetComponent<IPooledObjects>();

        if (pooledObjects != null)
        {
            pooledObjects.OnObjectSpawn();
        }

        poolDictionary[tag].Enqueue(objectToSpawn);//sätter tillbaka kulan i kön

        return objectToSpawn;
    }
}
