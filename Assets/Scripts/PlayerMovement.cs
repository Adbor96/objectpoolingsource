﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12;
    public int health = 3;
    public float invinTime = 2; //how long the player is invincible after taking damage
    public bool isInvincible = false;
    public bool isDead = false;

    public Ray playerRay;
    public RaycastHit playerRayHit = new RaycastHit();

    [SerializeField]
    private float iFrameDuration;

    [SerializeField]
    private float iFrameDeltaTime;

    [SerializeField]
    private GameObject model;
    private Vector3 modelScale;
    // Start is called before the first frame update
    void Start()
    {
        iFrameDuration = 1.5f;
        iFrameDeltaTime = 0.15f;
        modelScale = model.transform.localScale;
    }


    public void PlayerStart()
    {
        speed = 12;
        health = 3;
        gameObject.GetComponent<CharacterController>().enabled = true;
        model.GetComponent<MeshRenderer>().enabled = true;
        isInvincible = true;
        isDead = false;
    }
    public void LoseHealth(int amount)
    {
        //if (isInvincible) return;
        health -= amount;
        if (health <= 0)
        {
            StartCoroutine(DieAndRespawn());
            Debug.Log("Death");
            return;
        }
        StartCoroutine(IFrames());
    }
    private void BulletEvent_OnPlayerImpact(object sender, EventArgs e)
    {
        Debug.Log("Impact");
        LoseHealth(1);
    }

    // Update is called once per frame
    void Update()
    {
        float x = Input.GetAxis("Horizontal");
        float y = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.up * y;

        controller.Move(move * speed * Time.deltaTime);

        Bullet bulletEvent = GameObject.FindObjectOfType<Bullet>();
        if (bulletEvent != null)
        {
            bulletEvent.OnPlayerImpact += BulletEvent_OnPlayerImpact;
        }
        else
        {
            Debug.Log("Error");
        }
        //damage
        /*if (isInvincible) //fråga: vad är skillnaden mellan att använda if (isInvincible) och while (isInvincible)?
        {
            invinTime -= Time.deltaTime;
            if (invinTime <= 0)
            {
                invinTime = 2;
                isInvincible = false;
            }
        }*/
        //ray
        if (Input.GetKeyDown(KeyCode.E))
        {
            /*playerRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(playerRay, out playerRayHit, 1.5f))
            {
                IInteracteable hitObject = playerRayHit.collider.GetComponent<IInteracteable>();
                if (hitObject != null)
                {
                    hitObject.Interact();
                }
            }*/
        }
    }
    //I-frames
    private IEnumerator IFrames()
    {
        Debug.Log("Player is invincible");
        isInvincible = true;

        for (float i = 0; i < iFrameDuration; i += iFrameDeltaTime)
        {
            // Alternate between 0 and 1 scale to simulate flashing
            if (model.transform.localScale == modelScale)
            {
                ScaleModelTo(Vector3.zero);
            }
            else
            {
                ScaleModelTo(modelScale);
            }

            yield return new WaitForSeconds(iFrameDeltaTime);
        }

        isInvincible = false;
        ScaleModelTo(modelScale);
        Debug.Log("Player is not invincible.");
    }
    void TriggerIFrames()
    {
        if (!isInvincible)
        {
            StartCoroutine(IFrames());
        }
    }
    private void ScaleModelTo(Vector3 scale)
    {
        model.transform.localScale = scale;
    }
    //death
    void Death()
    {
        isDead = true;
        model.GetComponent<MeshRenderer>().enabled = false;
        speed = 0;
        gameObject.GetComponent<CharacterController>().enabled = false;
    }
    private IEnumerator DieAndRespawn()
    {
        Death();
        yield return new WaitForSeconds(3);

        PlayerStart();
    }
}


