﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletSpawner : MonoBehaviour
{
    BulletPooler bulletPooler;
    
    public float reloadTime = 0.2f;

    private void Start()
    {
        bulletPooler = BulletPooler.Instance;
    }
    private void FixedUpdate()
    {
        reloadTime -= Time.deltaTime;
        if (reloadTime <= 0)
        {
            bulletPooler.SpawnFromPoll("Bullet", transform.position, Quaternion.identity);
            reloadTime = 0.2f;
        }
    }
}
